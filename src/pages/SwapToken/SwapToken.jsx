/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from 'react'
import Web3 from 'web3'
import { chainlistRPC } from '../../api/chainlistRPC'
import Button from '../../components/Button/Button'
import CheckBox from '../../components/CheckBox/CheckBox'
import Logger from '../../components/Logger'
import { CHAIN_ID, INFURA_URL_LIST } from '../../constants/common'
import { useLog } from '../../contexts/logger.context'
import './swaptoken.scss'
import { ethers } from 'ethers'
import { Route, Token, Trade, TradeType, TokenAmount, WETH, Fetcher, Percent } from '@uniswap/sdk'
import ClearIcon from 'src/components/ClearIcon'
import Loading from 'src/components/Loading'
import axios from 'axios'
import UNISWAP_ROUTER from './abi/UNISWAP-ROUTER-ABI.json'

console.log('UNISWAP_ROUTER: ', UNISWAP_ROUTER)

// const ChainId = 42161
// const arbTokens = {
//   TOKEN_ADDRESS: '0x123456789abcdef'
// }
// const arbProviderUrl = 'https://arb1.arbitrum.io/rpc'

const privateKey = process.env.REACT_APP_PRIVATE_KEY_WALLET


function SwapToken() {
  const [bestPrice, setBestPrice] = useState(0)
  const [tokenContract, setTokenContract] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  const [account, setAccount] = useState(null)
  const { logContent, handleLogContent, resetLoggerMessage } = useLog()
  const messagesEndRef = useRef(null)
  const [tokenInfo, setTokenInfo] = useState({
    address: '',
    ABI: '',
    infuraUrl: INFURA_URL_LIST[2].url
  })

  useEffect(() => {
    (async () => {
      // const response = await axios.get('http://103.57.223.118:8888/sanctum/csrf-cookie')

      // console.log('response', response.headers['Set-Cookie'])
      // const cookieName = '...';

      // const cookie = (xsrfToken.headers['set-cookie'])
      //   .find(cookie => cookie.includes(cookieName))
      //   ?.match(new RegExp(`^${cookieName}=(.+?);`))
      //   ?.[1];
      // console.log('xsrfToken: ', xsrfToken)

      // const result = await axios.post('http://103.57.223.118:8888/api/user/login', {
      //   headers: {
      //     'X-XSRF-TOKEN': 'eyJpdiI6ImN6QVg2Q0VIM2g5VWZKejhCaXpudFE9PSIsInZhbHVlIjoiUUozTmtqL1NuSTdaRm5zWC9iSzJtbnZFZFphZkRSdmpibWlWb01tLzhBOGNQRHVBbE85YnNvM1JST1ZyTHRyNkJJSFB2ZFh6TkM1UmpXc0pIYWlhYkdjQjNYTzBZbWVBVmhrWU16Q2lld2VPNy9tTkJ2YXRrOFVRK3huajV6dmQiLCJtYWMiOiI0ZGQ0MjA3Mzk4ZGU1YTEzMzM0NDY2NWZmOWQ2MGFhZTQyZWE5ZTUxMWEwN2FjMjhmMTIxMDFlNWVjZWZhMzg0IiwidGFnIjoiIn0%3D'
      //   }
      // }, {
      //   username: 'abcxyz1080@gmail.com',
      //   password: '12345678'
      // })


      const result = await axios.get('http://103.57.223.118:8888/sanctum/csrf-cookie',
        { headers: { 'Content-Type': 'application/json' }, withCredentials: true })
      // const result = await axios.post('http://103.57.223.118:8888/api/user/login')
    })()
  }, [])

  const [RPCList, setRPCList] = useState([])

  const [config, setConfig] = useState({
    buyTimes: 1,
    enableStoploss: 0,
    enableTokenBuy: 0,
    ttokenBuy: 1,
    sstoploss: 0,
    decimals: 18,
    bnbAmount: 0.0001,
    speedms: 0,
    secWait: 1,
    maxSlippage: 10,
    gwei: 10,
    addedUrl: '',
    enableProfitsell: 1,
    pprofitSell: 10,
    enableWaiting: 1,
    liqBefore: 1,
    approveToken: 1,
    enablePriceBuying: 1,
    enablePriceSelling: 1,
    onlySell: 0,
    gasUsage: 500000,
    sellSlippage: 40,
    priceBuy: 2,
    priceSell: 0.5,
    sellTimesEnable: 0,
    sellTimes: 1,
    version: 2,
    retryWhenerror: '0',
    amountThreads: 1,
    getClipboard: 1
  })

  // const network = "homestead"
  // // Create a Web3 instance
  // const provider = ethers.getDefaultProvider(network, {
  //   etherscan: 'ZAKA56NAWHQYI8INX756T2W65QHR3BCTEF',
  //   infura: process.env.REACT_APP_INFURA_API_KEY
  // })
  // console.log('provider: ', provider)

  // const web3 = new Web3(tokenInfo.infuraUrl)
  // const provider = new Web3.providers.HttpProvider(tokenInfo.infuraUrl)
  // const web3 = new Web3('https://rpc.ankr.com/arbitrum')
  // Connect to Arbitrum Ethereum provider

  const swapEtherForToken = async () => {
    // Địa chỉ URL của QuickNode
    const quickNodeUrl = 'https://skilled-yolo-model.arbitrum-mainnet.discover.quiknode.pro/74fbb23790178839a9e9e07f4afffd2673ae4d69'

    // Tạo đối tượng Web3 với QuickNode làm nhà cung cấp
    const web3 = new Web3(new Web3.providers.HttpProvider(quickNodeUrl))

    // const { gasLimit } = await web3.eth.getBlock('latest')
    // const gasPrice = 20
    // console.log('gasLimit: ', gasLimit)
    // const gasPrice = await web3.eth.getGasPrice()

    // Địa chỉ của Router contract của Uniswap trên mạng Arbitrum
    // const routerAddress = '0x68b3465833fb72A70ecDF485E0e4C7bD8665Fc45'
    const routerAddress = '0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45'

    // Địa chỉ của token muốn hoán đổi sang
    // const tokenAddress = '0xD77B108d4f6cefaa0Cae9506A934e825BEccA46E'
    const tokenAddress = '0x82aF49447D8a07e3bd95BD0d56f35241523fBab1'

    // Số lượng ETH muốn hoán đổi
    const ethAmount = '0.0001'

    // Số lượng gas muốn sử dụng
    // const gasAmount = '0.01'
    const gasLimit = 500000
    // const gasPrice = 20
    const gasPrice = 20

    const gasAmount = gasPrice * gasLimit

    // Giao dịch (transaction) để thực hiện hoán đổi ETH sang token
    const transaction = {
      from: '0x6852Ba39D838B49c292d309b2321604128e8FaC5', // Địa chỉ ví của bạn
      to: routerAddress, // Địa chỉ của Router contract của Uniswap
      data: web3.eth.abi.encodeFunctionCall(
        {
          name: 'swapETHForExactTokens',
          type: 'function',
          inputs: [
            { type: 'uint256', name: 'amountOutMin' },
            { type: 'address[]', name: 'path' },
            { type: 'address', name: 'to' },
            { type: 'uint256', name: 'deadline' }
          ]
        },
        [
          '0', // amountOutMin: Giá trị tối thiểu của token muốn nhận
          [tokenAddress], // path: Mảng địa chỉ của token đầu vào và token đầu ra
          '0x6852Ba39D838B49c292d309b2321604128e8FaC5', // to: Địa chỉ ví nhận token
          Math.floor(Date.now() / 1000) + 60 * 10 // deadline: Thời gian hết hạn của giao dịch (hiện tại + 10 phút)
        ]
      ),
      value: web3.utils.toWei(ethAmount, 'ether'), // Số lượng ETH muốn hoán đổi
      gas: gasAmount // Số lượng gas muốn sử dụng
    }

    // Ký và gửi giao dịch
    web3.eth.accounts.signTransaction(transaction, process.env.REACT_APP_PRIVATE_KEY_WALLET)
      .then((signedTx) => {
        web3.eth.sendSignedTransaction(signedTx.rawTransaction)
          .on('transactionHash', (txHash) => {
            console.log(`Transaction hash: ${txHash}`)
          })
          .on('receipt', (receipt) => {
            console.log('Transaction receipt:', receipt)
          })
          .on('error', (error) => {
            console.error('Transaction error:', error)
          })
      })
      .catch((error) => {
        console.error('Sign transaction: ', error.message)
      })
  }

  const swapEtherForToken2 = async () => {
    const arbProvider = new ethers.providers.InfuraProvider('mainnet', 'f637cdaa382546ddb75eaf8ae149646b')
    const web3 = new Web3(arbProvider)

    const routerAddress = '0x68b3465833fb72A70ecDF485E0e4C7bD8665Fc45'
    const routerContract = new web3.eth.Contract(
      UNISWAP_ROUTER.abi,
      routerAddress)

    // Step 3: Define input and output tokens, and swap amounts
    const inputToken = '0x82aF49447D8a07e3bd95BD0d56f35241523fBab1'; // WETH token address
    const outputToken = '0xD77B108d4f6cefaa0Cae9506A934e825BEccA46E'; // WINR token address
    const amountIn = web3.utils.toWei('0.0001', 'ether')

    // Step 4: Set up swap parameters
    const amountOutMinimum = 0; // Minimum amount of output tokens accepted
    const deadline = Math.floor(Date.now() / 1000) + 60 * 10; // Deadline (10 minutes from now)
    const recipient = '0x6852Ba39D838B49c292d309b2321604128e8FaC5'; // Address to receive the swapped tokens

    // Step 5: Encode the exactInputSingle function call
    const functionSignature = 'exactInputSingle((address,address,uint24,uint256,uint256,uint256,uint160))'
    const data = web3.eth.abi.encodeFunctionSignature(functionSignature) +
      web3.eth.abi.encodeParameters(
        ['address', 'address', 'uint24', 'uint256', 'uint256', 'uint256', 'uint160'],
        [inputToken, outputToken, 3000, amountIn, amountOutMinimum, deadline, recipient]
      ).slice(2)

    console.log('data: ', data)

    // Step 6: Estimate gas for the transaction
    // const gasEstimate = await routerContract.methods.exactInputSingle(data).estimateGas({ from: '0x6852Ba39D838B49c292d309b2321604128e8FaC5' }); // Replace with your actual sender address
    // console.log('gasEstimate: ', gasEstimate)

    // Step 7: Build the transaction object
    const txObj = {
      from: '0x6852Ba39D838B49c292d309b2321604128e8FaC5', // Replace with your actual sender address
      to: routerAddress,
      gas: 5000,
      data: data
    };

    // Step 8: Sign and send the transaction
    const signedTx = await web3.eth.accounts.signTransaction(txObj, process.env.REACT_APP_PRIVATE_KEY_WALLET); // Replace with your actual private key
    const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
  }

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' })
  }

  const handleCheckToken = async () => {
    // const contractABI = JSON.parse(tokenInfo?.ABI)
    // const contractAddress = tokenInfo?.address
    // const myContract = await new web3.eth.Contract(contractABI, contractAddress)
    // const symbol = await myContract.methods.symbol?.().call()
    // const name = await myContract.methods.name?.().call()
    // // eslint-disable-next-line no-console
    // if (symbol || name) console.log(`Token is: ${name}(${symbol})`)
  }

  useEffect(() => {
    scrollToBottom()
  }, [logContent])

  // useEffect(() => {
  //   ; (async () => {
  //     const web3 = new Web3('https://rpc.ankr.com/arbitrum')
  //     const balance = await web3.eth.getBalance(account.address)
  //     console.log('balance: ', web3.utils.fromWei(balance))
  //   })()
  // }, [])

  // useEffect(() => {
  //   ; (async () => {
  //     const account = web3.eth.accounts.privateKeyToAccount(
  //       process.env.REACT_APP_PRIVATE_KEY_WALLET
  //     )
  //     delete account.privateKey
  //     setAccount(account)
  //   })()
  // }, [])

  // useEffect(() => {
  //   const getRPCs = async () => {
  //     let chainID
  //     for (let [key, value] of Object.entries(CHAIN_ID)) {
  //       if (key === tokenInfo.infuraUrl) {
  //         chainID = value
  //       }
  //     }
  //     if (!chainID) setRPCList([])
  //     else {
  //       const response = await chainlistRPC.getRPCList(chainID)
  //       setRPCList(response.pageProps.chain.rpc)
  //     }
  //   }
  //   getRPCs()
  // }, [tokenInfo.infuraUrl])

  const handleChangeConfig = e => {
    setConfig({
      ...config,
      [e.target.name]: e.target.value
    })
  }

  const handleSwapToken = async () => {
    // const { gasLimit } = await web3.eth.getBlock('latest')
    // const gasPrice = await web3.eth.getGasPrice()

    // const amountInEth = 1
    // const amountInWei = web3.utils.toWei(amountInEth.toString(), "ether");

    // // Build swap transaction
    // const swapTx = tokenContract.methods.swapExactETHForTokens(
    //   0, // slippage tolerance, set to 0 for no tolerance
    //   [web3.utils.fromAscii("<TOKEN_PATH>")], // token path, e.g. ETH to token: [ETH, token]
    //   account.address,
    //   Math.floor(Date.now() / 1000) + 60 * 10 // deadline, 10 minutes from now
    // )

    // // Send swap transaction
    // await swapTx.send({
    //   from: account,
    //   to: "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D",
    //   value: amountInWei,
    //   gasPrice: web3.utils.toWei("30", "gwei"), // set gas price as needed
    // })

    console.log("Swap completed successfully");
  }

  return (
    <main className="main-content position-relative border-radius-lg">
      <div className="container-fluid py-4">
        <div className="row">
          <div className="col-12">
            <div className="card mb-4">
              <div className="card-header pb-0 d-flex justify-content-between align-items-center">
                <h6>SWAP TOKEN</h6>
              </div>
              <div className="card-body p-4 d-flex">
                <div className="col-8 form-swap pe-4 ps-1">
                  <div className="token-controller d-flex justify-content-between mb-3">
                    <div>
                      <p className="h6 mb-0">Token Address</p>
                      <div className="input-group">
                        <input
                          required
                          className="form-control"
                          style={{}}
                          type="text"
                          onChange={e =>
                            setTokenInfo({
                              ...tokenInfo,
                              address: e.target.value
                            })
                          }
                        />
                        <Button
                          onClick={handleCheckToken}
                          className="btn btn-secondary mb-0"
                          type="button"
                          id="button-addon2"
                        >
                          Check token
                        </Button>
                      </div>
                    </div>
                    <div>
                      <p className="h6 mb-0">Contract ABI</p>
                      <div className="input-group">
                        <input
                          className="abi-input form-control"
                          style={{ minWidth: '8rem' }}
                          type="text"
                          onChange={e =>
                            setTokenInfo({
                              ...tokenInfo,
                              ABI: e.target.value
                            })
                          }
                        />
                      </div>
                    </div>
                    <div>
                      <p className="h6 mb-0">Select network</p>
                      <select
                        className="form-select network"
                        aria-label="Default select example"
                        onChange={e =>
                          setTokenInfo({
                            ...tokenInfo,
                            infuraUrl: e.target.value
                          })
                        }
                      >
                        <option disabled selected value="">
                          Select Network
                        </option>
                        {INFURA_URL_LIST.map(ifr => (
                          <option key={ifr.id} value={ifr.url}>
                            {ifr.name}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>

                  <div className="d-flex justify-content-between mb-3">
                    <div className="d-flex mb-3 align-items-center">
                      <div className="dropdown me-2">
                        <button
                          className="btn bg-gradient-light dropdown-toggle m-0"
                          type="button"
                          id="dropdownMenuButton"
                          data-bs-toggle="dropdown"
                          aria-expanded="false"
                        >
                          Select RPC
                        </button>
                        <ul
                          className="dropdown-menu"
                          aria-labelledby="dropdownMenuButton"
                          style={{ maxHeight: '20rem', overflowY: 'scroll' }}
                        >
                          {RPCList.map((e, i) => (
                            <li key={i}>
                              <span className="dropdown-item m-0">
                                {e.url};
                              </span>
                            </li>
                          ))}
                        </ul>
                      </div>
                      <Button
                        className="btn bg-gradient-light me-2 mb-0"
                        children="Check latency"
                      />
                      <Button
                        className="btn bg-gradient-light mb-0"
                        children="Check all latency"
                      />
                      <div className="dropdown mx-4">
                        <button
                          className="btn bg-gradient-warning dropdown-toggle m-0"
                          type="button"
                          id="dropdownMenuButton"
                          data-bs-toggle="dropdown"
                          aria-expanded="false"
                        >
                          Select Dex
                        </button>
                        <ul
                          className="dropdown-menu"
                          aria-labelledby="dropdownMenuButton"
                        >
                          <li>
                            <span className="dropdown-item m-0">Uniswap</span>
                          </li>
                          <li>
                            <span className="dropdown-item m-0">
                              Pancake Swap
                            </span>
                          </li>
                          <li>
                            <span className="dropdown-item m-0">SushiSwap</span>
                          </li>
                        </ul>
                      </div>
                      <div className="d-flex flex-column align-items-center">
                        <CheckBox title="V1" id="1" />
                        <CheckBox title="V2" id="2" />
                        <CheckBox title="V3" id="3" />
                      </div>
                    </div>
                  </div>
                  <hr className="horizontal dark m-3" />
                  <div className="mb-4">
                    <div className="d-flex justify-content-between mb-3">
                      <CheckBox
                        name="ttokenBuy"
                        writable
                        title="Token amount buy"
                        id="tokenamount"
                        defaultValue={config.ttokenBuy}
                        onChange={handleChangeConfig}
                      />
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <CheckBox
                        name="priceBuy"
                        writable
                        title="Only buy if token price is less than ($)"
                        id="onlybuy"
                        defaultValue={config.priceBuy}
                        onChange={handleChangeConfig}
                      />
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <CheckBox
                        name="maxSlippage"
                        writable
                        title="Max slippage"
                        id="maxslippage"
                        defaultValue={config.maxSlippage}
                      />
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <CheckBox
                        name="gwei"
                        writable
                        title="GWEI to use for trade"
                        id="gwei"
                        defaultValue={config.gwei}
                        onChange={handleChangeConfig}
                      />
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <CheckBox
                        name="amountThreads"
                        writable
                        title="Amount of threads running simultaneously (1 or more)"
                        id="amount-threads"
                        defaultValue={config.amountThreads}
                        onChange={handleChangeConfig}
                      />
                    </div>
                    <div className="d-flex justify-content-between mb-3">
                      <CheckBox
                        name="secWait"
                        writable
                        title="Amount of seconds to wait till sell after buying"
                        id="amount-second"
                        defaultValue={config.secWait}
                        onChange={handleChangeConfig}
                      />
                    </div>
                  </div>
                  <hr className="horizontal dark m-3" />
                  <div className="d-flex justify-content-between">
                    <div className="w-65">
                      <div className="d-flex justify-content-between mb-3">
                        <CheckBox
                          name="sellSlippage"
                          writable
                          title="Custom slippage for selling(%, 1=1%)"
                          id="custom-slippage"
                          defaultValue={config.sellSlippage}
                          onChange={handleChangeConfig}
                        />
                      </div>
                      <div className="d-flex justify-content-between mb-3">
                        <CheckBox
                          name="sstoploss"
                          writable
                          title="Let the bot sell when my loss is this much % (stoploss)"
                          id="stop-loss"
                          defaultValue={config.sstoploss}
                          onChange={handleChangeConfig}
                        />
                      </div>
                      <div className="d-flex justify-content-between mb-3">
                        <CheckBox
                          name="secWait"
                          writable
                          title="Amount of seconds to wait till sell after buying"
                          id="await"
                          defaultValue={config.secWait}
                          onChange={handleChangeConfig}
                        />
                      </div>
                      <div className="d-flex justify-content-between mb-3">
                        <CheckBox
                          name="priceSell"
                          writable
                          title="Sell when token price is this much % higher than start"
                          defaultValue={config.priceSell}
                          onChange={handleChangeConfig}
                          id="sell-higher"
                        />
                      </div>
                      <div className="d-flex justify-content-between mb-3">
                        <CheckBox
                          writable
                          title="Sell when token price is more than ($)"
                          id="sell-more-than"
                        />
                      </div>
                      <div className="d-flex justify-content-between mb-3">
                        <CheckBox
                          id="only-sell"
                          title="Only sell token, don't buy"
                        />
                      </div>
                      <div className="d-flex align-items-center justify-content-between">
                        <div className="d-flex align-items-center">
                          <p className="h6 mb-0 me-2">Max Gas</p>
                          <input
                            name="gasUsage"
                            className="form-control"
                            type="number"
                            style={{ maxWidth: '15rem' }}
                            defaultValue={config.gasUsage}
                            onChange={handleChangeConfig}
                          />
                        </div>
                        <div className="d-flex align-items-center">
                          <Button
                            className={`start btn btn-outline-success px-4 mx-4 mb-0 ${isLoading ? 'bg-success' : ''
                              }`}
                            children={
                              isLoading ? <Loading size="small" /> : 'START'
                            }
                            onClick={handleSwapToken}
                          />
                          <Button
                            className={`start btn btn-outline-success px-4 mx-4 mb-0 ${isLoading ? 'bg-success' : ''
                              }`}
                            children={
                              isLoading ? <Loading size="small" /> : 'START 1'
                            }
                            onClick={swapEtherForToken}
                          />

                          <Button
                            className={`start btn btn-outline-success px-4 mx-4 mb-0 ${isLoading ? 'bg-success' : ''
                              }`}
                            children={
                              isLoading ? <Loading size="small" /> : 'START 2'
                            }
                            onClick={swapEtherForToken2}
                          />



                          <Button
                            className="stop btn btn-outline-danger px-4 mb-0"
                            children="STOP"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="w-35 ps-4">
                      <div className="mb-3">
                        <p className="h6">Force buy </p>
                        <Button
                          className="btn btn-primary mb-4 me-2 w-100"
                          children="Force buy"
                        />
                      </div>
                      <div className="container text-center ps-3 gap-1">
                        <p className="h6">Force sell </p>
                        <div className="row">
                          <Button
                            className="col-6 btn btn-primary"
                            children="100%"
                          />
                          <Button
                            className="col-6 btn btn-primary"
                            children="50%"
                          />
                        </div>
                        <div className="row">
                          <Button
                            className="col-6 btn btn-primary "
                            children="25%"
                          />
                          <Button
                            className="col-6 btn btn-primary"
                            children="10%"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-4">
                  <div className="d-flex justify-content-end">
                    <ClearIcon
                      className="cursor-pointer"
                      onClick={resetLoggerMessage}
                    />
                  </div>
                  <div
                    className="mt-2 border border-light rounded p-3 bg-light text-dark"
                    style={{ height: '80vh', overflowY: 'auto' }}
                  >
                    <Logger />
                    <div ref={messagesEndRef} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  )
}

export default SwapToken
